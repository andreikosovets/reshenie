$('.bxslider').bxSlider({
    auto: true,
    controls: false
});

$('.job-img, .job').hover(function () {
    $(this).parent(".vacancy").find(".job-img").attr('src','img/circle_hov.jpg')
}, function () {
    $(this).parent(".vacancy").find(".job-img").attr('src','img/circle_pas.jpg')
}).click(function () {
    $(this).parent(".vacancy").toggleClass('true-open');
});

$("#doc-doc").on('click', function () {
   $("#doc2").toggleClass("open")
});


$("#someBtn").hover(function(){
        $(this).removeClass("money-m");
        $(this).addClass("money-m2").animate({opacity: 1}, 300);
    }, function(){
        $(this).removeClass("money-m2");
        $(this).addClass("money-m");
    });
    $("#someBtn2").hover(function(){
        $(this).removeClass("money-m");
        $(this).addClass("money-m2").animate({opacity: 1}, 300);
    }, function(){
        $(this).removeClass("money-m2");
        $(this).addClass("money-m");
    });


    // For_contact_page
                  $(document).ready(function () {
                    ymaps.ready(init);
                })


                function init() {
                    //point = [55.803125, 37.490371];
                    point = [56.314139, 38.136327];
                    myMap = new ymaps.Map("cont_map", {
                        //center: [55.798326, 37.491251],
                        center: [56.314139, 38.136327],
                        zoom: 15
                    }, {suppressMapOpenBlock: true});
                    myMap.behaviors.disable('scrollZoom');

                    /*var line = new ymaps.GeoObject({
                     geometry: {
                     type: "LineString",
                     coordinates: [
                     point,
                     [55.80037, 37.492753],
                     [55.797336, 37.489513],
                     [55.796369, 37.487968],
                     [55.793806, 37.492989]
                     ]
                     }
                     }, {
                     strokeWidth: 5,
                     strokeOpacity: 0.8,
                     strokeColor: "#849AC7"
                     });*/
                    var placemark = new ymaps.Placemark(point, {});
                    myMap.geoObjects
                        //.add(line)
                        .add(placemark);
                }
   


$(document).ready( function() {
    $(".file-upload input[type=file]").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#filename").show();
        $("#filename .text-file").text(filename);
    });
    $(".filename span:not(.text-file)").click(function(){
        $(".file-upload input[type=file]").val('').change();
        $(".filename").hide();
    });
});

function randomNumber (m,n){
    m = parseInt(m);
    n = parseInt(n);
    return Math.floor( Math.random() * (n - m + 1) ) + m;
}
var timeOut = 500;
setInterval(function(){
    do{
        var number = randomNumber(1,4);
    }while('/img/img'+number+'.png' == $('.apple_img').attr('src'));
    $('.apple_img').attr('src', '/img/img'+number+'.png');
},timeOut)